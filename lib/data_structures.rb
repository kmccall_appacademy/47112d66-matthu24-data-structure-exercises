# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  return (arr.max-arr.min)
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  if arr.sort == arr
    return true
  else
    return false
  end
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = ["a","e","i","o","u"]
  count_vowels = 0
  str.each_char do |char|
    if vowels.include?(char.downcase)
      count_vowels += 1
    end
  end
  count_vowels
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  vowels = ["a","e","i","o","u"]
  str = str.downcase
  str.each_char do |char|
    if vowels.include?(char)
      str.delete!(char)
    end
  end
  str
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  string_integer = int.to_s
  descending_digits = []
  string_integer.each_char do |char|
    descending_digits << char
  end
  descending_digits.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  seen = []
  str.downcase.each_char do |char|
    if seen.include?(char)
      return true
    else
      seen << char
    end
  end
  return false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  phone_number = ""
  arr.each_with_index do |num,index|
    if index == 0
      phone_number += "("
    elsif index == 3
      phone_number += ") "
    elsif index == 6
      phone_number += "-"
    end
    phone_number += num.to_s
  end
  phone_number
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  arr = []
  idx = 0
  while idx < str.length
    arr << str[idx].to_i
    idx += 2
  end
  arr.max-arr.min
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  index = offset%arr.length
  arr.drop(index) + arr.take(index)
end
